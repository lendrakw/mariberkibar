﻿using System;
using Kalkulator;

namespace Learning001
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 kalkulator = new Class1();

            Console.WriteLine("Hello World!!!!");
            Console.WriteLine("Muhammad Omar Mochtar");
            Console.Write("Press Y/N?");

            bool kondisiSaatIni = Console.ReadKey().Key == ConsoleKey.Y;
            //Percabangan - Branch
            if (kondisiSaatIni)
            {
                Console.WriteLine("Percobaan 1");
            }
            else
            {
                Console.WriteLine("Percobaan 2");
            }

            //Perulangan - Looping

            Console.WriteLine();

            int banyaknyaLooping = 0;

            Console.Write("Masukan angka integer ? ");
            int.TryParse(Console.ReadLine(), out banyaknyaLooping);

            for (int i = 0; i < banyaknyaLooping; i++)
            {
                //Console.Write("Looping ");
                Console.WriteLine(i + 1);
            }


            Console.Write("Perkalian: 2x2= ");
            Console.WriteLine(kalkulator.perkalian(2, 2));
        }


    }
}
